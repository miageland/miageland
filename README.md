# Doc Devops

# **Projet DevOps instructions**

LUSSIEZ Yohan

PORTEBOS Paco

M2 MIAGE ITN

## **Notes importantes**

Ce README explique les étapes que nous avons réalisé afin de tenter de déployer notre application web (front-end + back-end + base de données).

Nous avons bien réussi à déployer les containers, toutefois ces derniers ne sont pas accessibles via l’URL public au bon port. Le back-end et la base de données semblent également redémarrer en boucle.

Nous avons également essayé d’automatiser cela un maximum avec Terraform. Toutefois nous ne savons pas comment avec un seul fichier Terraform :

- Créer le Resource Group (Terraform)
- Créer le Container Registry (Terraform)
- Tag et pousser les images Docker sur le Registry
    - *Ne semble pas possible via Terraform*
- Déployer le Container group (Terraform)

Le réaliser en 2 fichiers semble compliqué car la première exécution de Terraform stocke des résultats de variables que le second fichier devrait importer. Une autre solution serait un script shell gérant le tout, mais cela nous semble hors-sujet et pas une solution propre.

Nous avons donc opté pour la solution suivante :

- Créer le Resource Group et le Container Registry via Terraform
- Tag et pousser les images Docker sur le registry via l’invite de commande
- Déployer le container group via l’invite de commande (1 commande)

## **Introduction**

Ce guide décrit les étapes nécessaires pour préparer, tester et déployer des images Docker sur Azure Container Registry (ACR) en utilisant Microsoft Azure. Nous utiliserons les variables suivantes dans ce tutoriel :

- `miagelandResourceGroup`
- `miagelandregistry`
- `miagelandregistry.azurecr.io`
- `miageLandContainerGroup`

Si ces variables ne sont pas disponibles sur votre Microsoft Azure, vous pouvez les modifier lors de leur création.

## **Prérequis**

- Un compte Microsoft Azure.
- Docker installé sur votre machine locale.
- Azure CLI installé sur votre machine locale.

## **Étapes**

### **1. Clonage des dépôts Git**

1. Clonez le dépôt principal :
    
    ```bash
    git clone https://gitlab.com/miageland/miageland
    
    ```
    
2. Accédez au répertoire cloné :
    
    ```bash
    cd miageland
    
    ```
    
3. Clonez les dépôts front et back à l'intérieur du répertoire principal :
    
    ```bash
    git clone https://gitlab.com/miageland/miagelandfront
    git clone https://gitlab.com/miageland/miagelandback
    
    ```
    

### **2. Test en local**

1. Construisez les images Docker :
    
    ```bash
    docker compose build
    
    ```
    
2. Lancez les conteneurs Docker :
    
    ```bash
    docker compose up -d
    ```
    

![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled.png)

1. Ouvrez votre navigateur et accédez à `http://localhost:80` pour tester l'application web.

![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%201.png)

### **2. Création du resource group et du image registry sur Azure**

1. Connectez-vous à Azure avec vos identifiants :

```bash
az login

```

![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%202.png)

1. Initialiser Terraform

```yaml
terraform init

```

1. Exécuter Terraform et valider en entrant “yes”

```yaml
terraform apply

```

![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%203.png)

1. Noter le password affiché avec la commande ci-dessous :

```yaml
terraform output -json
```

![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%204.png)

1. Se connecter au registry tout juste crée :

```yaml
az acr login --name miagelandregistry

```

![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%205.png)

### **3. Poussée des images sur Azure Container Registry**

1. Notez les noms des 3 repositories correspondants en utilisant :
    
    ```bash
    docker images
    ```
    
    ![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%206.png)
    
2. Utilisez ces noms pour taguer les images :
    
    ```bash
    docker tag miageland-miage_land_back miagelandregistry.azurecr.io/back:v1
    docker tag miageland-miage_land_front miagelandregistry.azurecr.io/front:v1
    docker tag postgres:13 miagelandregistry.azurecr.io/db:v1
    
    ```
    
3. Poussez les images vers ACR :
    
    ```bash
    docker push miagelandregistry.azurecr.io/back:v1
    docker push miagelandregistry.azurecr.io/front:v1
    docker push miagelandregistry.azurecr.io/db:v1
    ```
    
    ![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%207.png)
    
4. Vérifiez que les images ont été poussées correctement :
    
    ```bash
    az acr repository list --name miagelandregistry --output table
    ```
    
    ![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%208.png)
    

### **4. Déploiement du groupe de containers**

1. Avec la valeur de **password** noté en 2.4, **remplacer la valeur imageRegistryCredentials.password** en bas du fichier **deploy-aci.yaml** :
    
    ```yaml
    apiVersion: 2019-12-01
    location: eastus
    name: miageLandContainerGroup
    properties:
      containers:
        - name: miagelandfront
          properties:
            image: miagelandregistry.azurecr.io/front:v1
            resources:
              requests:
                cpu: 1
                memoryInGb: 1.5
            ports:
              - port: 80
        - name: miagelandback
          properties:
            image: miagelandregistry.azurecr.io/back:v1
            resources:
              requests:
                cpu: 1
                memoryInGb: 1.5
            ports:
              - port: 8080
        - name: db
          properties:
            image: miagelandregistry.azurecr.io/db:v1
            resources:
              requests:
                cpu: 1
                memoryInGb: 1.5
            ports:
              - port: 5432
      osType: Linux
      ipAddress:
        type: Public
        ports:
          - protocol: tcp
            port: 80
          - protocol: tcp
            port: 8080
          - protocol: tcp
            port: 5432
      imageRegistryCredentials:
      - server: miagelandregistry.azurecr.io
        username: miagelandregistry
        password: **<password>**
    tags: {exampleTag: miageland}
    type: Microsoft.ContainerInstance/containerGroups
    
    ```
    
2. Déployez le groupe de containers :
    
    ```bash
    az container create --resource-group miagelandResourceGroup --file deploy-aci.yaml
    ```
    
    Exemple de réponse :
    
    ```yaml
    {
      "extendedLocation": null,
      "id": "/subscriptions/d4c14631-df25-4312-8420-cb694e6e5eb7/resourceGroups/miagelandResourceGroup/providers/Microsoft.ContainerInstance/containerGroups/miageLandContainerGroup",
      "identity": null,
      "kind": null,
      "location": "eastus",
      "managedBy": null,
      "name": "miageLandContainerGroup",
      "plan": null,
      "properties": {
        "containers": [
          {
            "name": "miagelandfront",
            "properties": {
              "configMap": {
                "keyValuePairs": {}
              },
              "environmentVariables": [],
              "image": "miagelandregistry.azurecr.io/front:v1",
              "instanceView": {
                "currentState": {
                  "detailStatus": "",
                  "startTime": "2024-06-18T14:37:18.913Z",
                  "state": "Running"
                },
                "events": [
                  {
                    "count": 1,
                    "firstTimestamp": "2024-06-18T14:36:49Z",
                    "lastTimestamp": "2024-06-18T14:36:49Z",
                    "message": "pulling image \"miagelandregistry.azurecr.io/front@sha256:34e142cdbee7ad2808a05d3d0516c9c5325567e80adae4653022156e4f62eccb\"",
                    "name": "Pulling",
                    "type": "Normal"
                  },
                  {
                    "count": 1,
                    "firstTimestamp": "2024-06-18T14:37:08Z",
                    "lastTimestamp": "2024-06-18T14:37:08Z",
                    "message": "Successfully pulled image \"miagelandregistry.azurecr.io/front@sha256:34e142cdbee7ad2808a05d3d0516c9c5325567e80adae4653022156e4f62eccb\"",
                    "name": "Pulled",
                    "type": "Normal"
                  },
                  {
                    "count": 1,
                    "firstTimestamp": "2024-06-18T14:37:18Z",
                    "lastTimestamp": "2024-06-18T14:37:18Z",
                    "message": "Started container",
                    "name": "Started",
                    "type": "Normal"
                  }
                ],
                "restartCount": 0
              },
              "ports": [
                {
                  "port": 80
                }
              ],
              "resources": {
                "requests": {
                  "cpu": 1.0,
                  "memoryInGB": 1.5
                }
              }
            }
          },
          {
            "name": "miagelandback",
            "properties": {
              "configMap": {
                "keyValuePairs": {}
              },
              "environmentVariables": [],
              "image": "miagelandregistry.azurecr.io/back:v1",
              "instanceView": {
                "currentState": {
                  "detailStatus": "",
                  "startTime": "2024-06-18T14:37:42.551Z",
                  "state": "Running"
                },
                "events": [
                  {
                    "count": 1,
                    "firstTimestamp": "2024-06-18T14:36:49Z",
                    "lastTimestamp": "2024-06-18T14:36:49Z",
                    "message": "pulling image \"miagelandregistry.azurecr.io/back@sha256:dd38bed528011686fad5eeb5af14029510467c6b55dfea44fd2d40a590dc3636\"",
                    "name": "Pulling",
                    "type": "Normal"
                  },
                  {
                    "count": 1,
                    "firstTimestamp": "2024-06-18T14:37:08Z",
                    "lastTimestamp": "2024-06-18T14:37:08Z",
                    "message": "Successfully pulled image \"miagelandregistry.azurecr.io/back@sha256:dd38bed528011686fad5eeb5af14029510467c6b55dfea44fd2d40a590dc3636\"",
                    "name": "Pulled",
                    "type": "Normal"
                  },
                  {
                    "count": 2,
                    "firstTimestamp": "2024-06-18T14:37:18Z",
                    "lastTimestamp": "2024-06-18T14:37:42Z",
                    "message": "Started container",
                    "name": "Started",
                    "type": "Normal"
                  },
                  {
                    "count": 1,
                    "firstTimestamp": "2024-06-18T14:37:30Z",
                    "lastTimestamp": "2024-06-18T14:37:30Z",
                    "message": "Killing container with id fcecb9b87d2b24856cbd9b91268886035e2921f9287e779abf663232fe1b1866.",
                    "name": "Killing",
                    "type": "Normal"
                  }
                ],
                "previousState": {
                  "detailStatus": "Error",
                  "exitCode": 1,
                  "finishTime": "2024-06-18T14:37:30.242Z",
                  "startTime": "2024-06-18T14:37:18.779Z",
                  "state": "Terminated"
                },
                "restartCount": 1
              },
              "ports": [
                {
                  "port": 8080
                }
              ],
              "resources": {
                "requests": {
                  "cpu": 1.0,
                  "memoryInGB": 1.5
                }
              }
            }
          },
          {
            "name": "db",
            "properties": {
              "configMap": {
                "keyValuePairs": {}
              },
              "environmentVariables": [],
              "image": "miagelandregistry.azurecr.io/db:v1",
              "instanceView": {
                "currentState": {
                  "detailStatus": "CrashLoopBackOff: Back-off restarting failed",
                  "state": "Waiting"
                },
                "events": [
                  {
                    "count": 1,
                    "firstTimestamp": "2024-06-18T14:36:49Z",
                    "lastTimestamp": "2024-06-18T14:36:49Z",
                    "message": "pulling image \"miagelandregistry.azurecr.io/db@sha256:8cca20536bc7788f6aec5eae7143bc25859f8de843af7f4e4cc476821e09c966\"",
                    "name": "Pulling",
                    "type": "Normal"
                  },
                  {
                    "count": 1,
                    "firstTimestamp": "2024-06-18T14:37:08Z",
                    "lastTimestamp": "2024-06-18T14:37:08Z",
                    "message": "Successfully pulled image \"miagelandregistry.azurecr.io/db@sha256:8cca20536bc7788f6aec5eae7143bc25859f8de843af7f4e4cc476821e09c966\"",
                    "name": "Pulled",
                    "type": "Normal"
                  },
                  {
                    "count": 2,
                    "firstTimestamp": "2024-06-18T14:37:19Z",
                    "lastTimestamp": "2024-06-18T14:37:38Z",
                    "message": "Started container",
                    "name": "Started",
                    "type": "Normal"
                  },
                  {
                    "count": 2,
                    "firstTimestamp": "2024-06-18T14:37:24Z",
                    "lastTimestamp": "2024-06-18T14:37:45Z",
                    "message": "Killing container with id eb81322a70ffaa0573890c3b4341b177f7552f6acb61f005c6f6e443dfdab387.",
                    "name": "Killing",
                    "type": "Normal"
                  }
                ],
                "previousState": {
                  "detailStatus": "Error",
                  "exitCode": 1,
                  "finishTime": "2024-06-18T14:37:45.31Z",
                  "startTime": "2024-06-18T14:37:38.683Z",
                  "state": "Terminated"
                },
                "restartCount": 1
              },
              "ports": [
                {
                  "port": 5432
                }
              ],
              "resources": {
                "requests": {
                  "cpu": 1.0,
                  "memoryInGB": 1.5
                }
              }
            }
          }
        ],
        "imageRegistryCredentials": [
          {
            "isDelegatedIdentity": false,
            "server": "miagelandregistry.azurecr.io",
            "username": "miagelandregistry"
          }
        ],
        "initContainers": [],
        "instanceView": {
          "events": [],
          "state": "Running"
        },
        "ipAddress": {
          "ip": "20.253.85.126",
          "ports": [
            {
              "port": 80,
              "protocol": "TCP"
            },
            {
              "port": 8080,
              "protocol": "TCP"
            },
            {
              "port": 5432,
              "protocol": "TCP"
            }
          ],
          "type": "Public"
        },
        "isCreatedFromStandbyPool": false,
        "isCustomProvisioningTimeout": false,
        "osType": "Linux",
        "provisioningState": "Succeeded",
        "provisioningTimeoutInSeconds": 1800,
        "sku": "Standard"
      },
      "resourceGroup": "miagelandResourceGroup",
      "sku": null,
      "tags": {
        "exampleTag": "miageland"
      },
      "type": "Microsoft.ContainerInstance/containerGroups"
    }
    ```
    
3. Retrouvez votre groupe de containers sur Azure :

![Untitled](Doc%20Devops%20c82f51731f07474fa7861cb6f3b42a9a/Untitled%209.png)

## **Sources**

[https://learn.microsoft.com/en-us/azure/container-instances/container-instances-tutorial-prepare-acr](https://learn.microsoft.com/en-us/azure/container-instances/container-instances-tutorial-prepare-acr)

[https://learn.microsoft.com/en-us/azure/container-instances/container-instances-tutorial-deploy-app](https://learn.microsoft.com/en-us/azure/container-instances/container-instances-tutorial-deploy-app)

[https://learn.microsoft.com/en-us/azure/container-instances/container-instances-multi-container-yaml](https://learn.microsoft.com/en-us/azure/container-instances/container-instances-multi-container-yaml)